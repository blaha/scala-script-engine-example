import scala.io.Source
import scala.util.Try
import javax.script.{ScriptEngine, ScriptException, ScriptEngineManager}
import java.io.File
//import scala.collection.JavaConversions._

object Main extends App {

//  def using[T <: { def close(): Unit }, R](source: T)(block: T => R): R = {
//    val result = Try(block(source))
//    source.close()
//    result.get
//  }

//  val migrationFiles = new File("migrations").listFiles().toList


  val engine: ScriptEngine = new ScriptEngineManager().getEngineByName("scala")
  val settings = engine.asInstanceOf[scala.tools.nsc.interpreter.IMain].settings
//  settings.embeddedDefaults[Cow]
  settings.usejavacp.value = true

  try {

    engine.put("n: Int", 10)
    engine.eval("1 to n foreach println")

  } catch {
    case e: ScriptException =>
      e.printStackTrace()
    case _: Throwable =>
      println("GODDAMNIT BEN!")
  }

//  import reflect.runtime.currentMirror
//  import scala.tools.reflect.ToolBox
//
//  val toolbox = currentMirror.mkToolBox()
//  import toolbox.u._
//
//  val code = q"""1 to 10 foreach(println)"""
//
//  val compiledCode = toolbox.compile(code)
//
//  compiledCode()

//  migrationFiles foreach { file =>
//
//    using(Source.fromFile(file)) { source =>
//      println(file.getName.toUpperCase + "\n")
//
//      val codes = source.mkString
//
//      println(codes)
//
//      engine.eval("1 to 10 foreach println") //source.reader())
//                                 }
//  }


}
