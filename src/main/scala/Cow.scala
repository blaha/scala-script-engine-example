abstract class Cow {
  def doit(s: String): Unit = println(s"CONSIDER IT DONE! $s")
}
