name := "moo"

version := "1.0-SNAPSHOT"

scalaVersion in ThisBuild := "2.11.6"

scalacOptions in ThisBuild ++= Seq(
    "-language:_",
    "-feature",
    "-unchecked",
    "-deprecation")

parallelExecution in ThisBuild := false

libraryDependencies ++= Seq(
    "org.scala-lang" % "scala-compiler" % "2.11.6",
    "org.scala-lang" % "scala-reflect" % "2.11.6",
    "org.scala-lang" % "scala-library" % "2.11.6")

// fork in ( Test, run ) := false